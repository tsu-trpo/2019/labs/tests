#!/bin/bash

set -e
set -x

git submodule update --init --recursive
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=release ..
cd ..

