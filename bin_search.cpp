#include "bin_search.h"
#include <algorithm>

int *bin_search(std::vector<int> &vec, int value)
{
    auto lower = std::lower_bound(vec.begin(), vec.end(), value);
    if (lower == vec.end()) {
        return nullptr;
    }

    if (*lower == value) {
        return &*lower;
    } else {
        return nullptr;
    }
}
