#include "find_substring.h"

size_t find_substring(const std::string &str, const std::string substr)
{
    return str.find(substr);
}
